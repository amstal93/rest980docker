#!/bin/bash

trap cleanup 1 2 3 6

cleanup()
{
  echo "Docker-Compose ... cleaning up."
  docker-compose down
  echo "Docker-Compose ... quitting."
  exit 1
}

set -e

if [ -z ${1+x} ]; then
    docker-compose -f ./docker-compose.yml up --build
else
    docker-compose -f ./docker-compose.${1}.yml up --build
fi
