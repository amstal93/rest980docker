FROM node:alpine

# Build arguments ...
ARG ARG_APP_VERSION 

# The channel or branch triggering the build.
ARG ARG_APP_CHANNEL

# The commit sha triggering the build.
ARG ARG_APP_COMMIT

# Build data
ARG ARG_BUILD_DATE

# REST980's branch to clone
ARG ARG_APP_BRANCH=master

# Basic build-time metadata as defined at http://label-schema.org
LABEL org.label-schema.build-date=${ARG_BUILD_DATE} \
    org.label-schema.docker.dockerfile="/Dockerfile" \
    org.label-schema.license="MIT" \
    org.label-schema.name="MiGoller" \
    org.label-schema.vendor="MiGoller" \
    org.label-schema.version=${ARG_APP_VERSION} \
    org.label-schema.description="REST980 for Docker" \
    org.label-schema.url="https://gitlab.com/users/MiGoller/projects" \
    org.label-schema.vcs-ref=${ARG_APP_COMMIT} \
    org.label-schema.vcs-type="Git" \
    org.label-schema.vcs-url="https://gitlab.com/MiGoller/rest980docker.git" \
    maintainer="MiGoller" \
    Author="MiGoller"

# Persist app-reladted build arguments
ENV APP_VERSION=${ARG_APP_VERSION} \
    APP_CHANNEL=${ARG_APP_CHANNEL} \
    APP_COMMIT=${ARG_APP_COMMIT} \
    APP_BUILD_DATE=${ARG_BUILD_DATE} \
    APP_BRANCH=${ARG_APP_BRANCH}

# Set default environment variables
ENV BLID= \
    PASSWORD= \
    ROBOT_IP= \
    FIRMWARE_VERSION=2 \
    ENABLE_LOCAL=yes \
    ENABLE_CLOUD=no \
    KEEP_ALIVE=no \
    SSL_KEY_FILE= \
    SSL_CERT_FILE= \
    BASIC_AUTH_USER= \
    BASIC_AUTH_PASS= \
    PORT=3000

# Node.js specific environment variables
ENV NODE_APP_INSTANCE=

# Install additional packages
RUN apk update && \
    apk add --no-cache \
        git

# Create app directory
RUN mkdir -p /opt/rest980
WORKDIR /opt/rest980

# Clonbe the repo and install the app
RUN git clone -b ${ARG_APP_BRANCH} https://github.com/koalazak/rest980.git /opt/rest980 && \
    npm i npm@latest -g && \
    npm install pm2 -g && \
    npm install && \
    npm audit fix --force

# Copy additional files...
COPY docker-entrypoint.sh pm2Rest980.yml ./
RUN chmod +x *.sh

EXPOSE ${PORT}

ENTRYPOINT ["/opt/rest980/docker-entrypoint.sh"]

# Start the REST interface!
# CMD [ "npm", "start" ]
CMD ["pm2-docker", "pm2Rest980.yml"]